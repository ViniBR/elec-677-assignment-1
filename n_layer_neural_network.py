__author__ = 'tan_nguyen'
import numpy as np
from sklearn import datasets, linear_model
import matplotlib.pyplot as plt



def generate_data(n_data):
    '''
    generate data
    :return: X: input data, y: given labels
    '''
    np.random.seed(0)
    X, y = datasets.make_moons(n_data, noise=0.20)
    return X, y

def generate_data_circles(n_data):
    '''
    generate data
    :return: X: input data, y: given labels
    '''
    np.random.seed(0)
    X, y = datasets.make_circles(n_samples=n_data, shuffle=True, noise=0.15, random_state=None, factor=0.4)
    return X, y


def plot_decision_boundary(pred_func, X, y):
    '''
    plot the decision boundary
    :param pred_func: function used to predict the label
    :param X: input data
    :param y: given labels
    :return:
    '''
    # Set min and max values and give it some padding
    x_min, x_max = X[:, 0].min() - .5, X[:, 0].max() + .5
    y_min, y_max = X[:, 1].min() - .5, X[:, 1].max() + .5
    h = 0.01
    # Generate a grid of points with distance h between them
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    # Predict the function value for the whole gid
    Z = pred_func(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    # Plot the contour and training examples
    plt.contourf(xx, yy, Z, cmap=plt.cm.Spectral)
    plt.scatter(X[:, 0], X[:, 1], c=y, cmap=plt.cm.Spectral)
    plt.show()

class NeuralNetwork(object):
    """
    This class builds and trains a neural network
    """

    def __init__(self, nn_input_dim, nn_hidden_dim, nn_output_dim, actFun_type, reg_lambda=0.01, seed=0):
        '''
        :param nn_input_dim: input dimension
        :param nn_hidden_dim: the number of hidden units
        :param nn_output_dim: output dimension
        :param actFun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        '''
        self.nn_input_dim = nn_input_dim
        self.nn_hidden_dim = nn_hidden_dim
        self.nn_output_dim = nn_output_dim
        self.actFun_type = actFun_type
        self.reg_lambda = reg_lambda

        # initialize the weights and biases in the network
        np.random.seed(seed)
        self.W1 = np.random.randn(self.nn_input_dim, self.nn_hidden_dim) / np.sqrt(self.nn_input_dim)
        self.b1 = np.zeros((1, self.nn_hidden_dim))
        self.W2 = np.random.randn(self.nn_hidden_dim, self.nn_output_dim) / np.sqrt(self.nn_hidden_dim)
        self.b2 = np.zeros((1, self.nn_output_dim))

    def actFun(self, z, type):
        '''
        actFun computes the activation functions
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: activations
        '''

        if type == 'Tanh':
            act = np.tanh(z)
        elif type == 'Sigmoid':
            act = 1 / (1 + np.exp(-z))
        elif type == 'ReLU':
            act = np.fmax(0, z)
        else:
            print("ERROR: Invalid ActFun Type (Probably a typo).")
            act = 0
        return act

    def diff_actFun(self, z, type):
        '''
        diff_actFun computes the derivatives of the activation functions wrt the net input
        :param z: net input
        :param type: Tanh, Sigmoid, or ReLU
        :return: the derivatives of the activation functions wrt the net input
        '''

        if type == 'Tanh':
            diff = 1 - np.power(np.tanh(z), 2)
        elif type == 'Sigmoid':
            sig = 1 / (1 + np.exp(-z))
            diff = sig * (1 - sig)
        elif type == 'ReLU':
            diff = 1 * (z > 0)
        else:
            print("ERROR: Invalid ActFun Type (Probably a typo).")
            diff = 0
        return diff

    def feedforward(self, X, actFun):
        '''
        feedforward builds a 3-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param actFun: activation function
        :return:
        '''

        self.z1 = X.dot(self.W1) + self.b1
        self.a1 = self.actFun(self.z1, self.actFun_type)
        self.z2 = self.a1.dot(self.W2) + self.b2
        exp_scores = np.exp(self.z2)
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)
        return None

    def calculate_loss(self, X, y):
        '''
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        '''
        num_examples = len(X)
        self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
        # Calculating the loss
        data_loss_aux = -np.log(self.probs[range(num_examples), y])
        data_loss = np.sum(data_loss_aux)
        # Add regulatization term to loss (optional)
        data_loss += self.reg_lambda / 2 * (np.sum(np.square(self.W1)) + np.sum(np.square(self.W2)))
        return (1. / num_examples) * data_loss

    def predict(self, X):
        '''
        predict infers the label of a given data point X
        :param X: input data
        :return: label inferred
        '''
        self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
        return np.argmax(self.probs, axis=1)

    def backprop(self, X, y):
        '''
        backprop implements backpropagation to compute the gradients used to update the parameters in the backward step
        :param X: input data
        :param y: given labels
        :return: dL/dW1, dL/b1, dL/dW2, dL/db2
        '''

        num_examples = len(X)
        delta3 = self.probs
        delta3[range(num_examples), y] -= 1
        dW2 = (self.a1.T).dot(delta3)
        db2 = np.sum(delta3, axis=0, keepdims=True)

        delta2 = delta3.dot(self.W2.T) * self.diff_actFun(self.z1, self.actFun_type)
        dW1 = np.dot(X.T, delta2)
        db1 = np.sum(delta2, axis=0)
        # dW2 /= num_examples
        # db2 /= num_examples
        # dW1 /= num_examples
        # db1 /= num_examples
        return dW1, dW2, db1, db2

    def fit_model(self, X, y, epsilon=0.001, num_passes=20000, print_loss=True):
        '''
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        '''
        # Gradient descent.
        for i in range(0, num_passes):
            # Forward propagation
            self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
            # Backpropagation
            dW1, dW2, db1, db2 = self.backprop(X, y)

            # Add regularization terms (b1 and b2 don't have regularization terms)
            dW2 += self.reg_lambda * self.W2
            dW1 += self.reg_lambda * self.W1

            # Gradient descent parameter update
            self.W1 += -epsilon * dW1
            self.b1 += -epsilon * db1
            self.W2 += -epsilon * dW2
            self.b2 += -epsilon * db2

            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.
            if print_loss and i % 1000 == 0:
                print("Loss after iteration %i: %f" % (i, self.calculate_loss(X, y)))

    def visualize_decision_boundary(self, X, y):
        '''
        visualize_decision_boundary plots the decision boundary created by the trained network
        :param X: input data
        :param y: given labels
        :return:
        '''
        plot_decision_boundary(lambda x: self.predict(x), X, y)


class n_layerNN(NeuralNetwork):

    def __init__(self, nn_nlayer_dim, nn_input_dim, nn_hidden_dim, nn_output_dim, actFun_type, reg_lambda=0.01, seed=0):
        '''
        :param nn_nlayer_dim: number of layers in the network, including in and output layers
        :param nn_input_dim: input dimension
        :param nn_hidden_dim: the number of hidden units
        :param nn_output_dim: output dimension
        :param actFun_type: type of activation function. 3 options: 'tanh', 'sigmoid', 'relu'
        :param reg_lambda: regularization coefficient
        :param seed: random seed
        '''
        self.nn_nlayer_dim = nn_nlayer_dim
        self.nn_input_dim = nn_input_dim
        self.nn_hidden_dim = nn_hidden_dim
        self.nn_output_dim = nn_output_dim
        self.actFun_type = actFun_type
        self.reg_lambda = reg_lambda
        self.W = []
        self.b = []
        self.z = []
        self.a = []
        self.loss_training = []
        self.loss_test =[]

        # initialize the weights and biases in the network
        np.random.seed(seed)
        self.W.append(np.random.randn(self.nn_input_dim, self.nn_hidden_dim) / np.sqrt(self.nn_input_dim))
        self.b.append(np.zeros((1, self.nn_hidden_dim)))
        if (self.nn_nlayer_dim > 3):
            for i in range(1, (self.nn_nlayer_dim - 2)):
                self.W.append(np.random.randn(self.nn_hidden_dim, self.nn_hidden_dim) / np.sqrt(self.nn_hidden_dim))
                self.b.append(np.zeros((1, self.nn_hidden_dim)))

        self.W.append(np.random.randn(self.nn_hidden_dim, self.nn_output_dim) / np.sqrt(self.nn_hidden_dim))
        self.b.append(np.zeros((1, self.nn_output_dim)))

    def feedforward(self, X, actFun):
        '''
        feedforward builds a n-layer neural network and computes the two probabilities,
        one for class 0 and one for class 1
        :param X: input data
        :param actFun: activation function
        :return:
        '''

        self.z = []
        self.a = []

        #Calculate z and a for the first hidden layer:
        self.z.append(X.dot(self.W[0]) + self.b[0])
        self.a.append(self.actFun(self.z[0], self.actFun_type))

        #Calculate z and a for the other hidden layers, if existent, and output.
        for i in range(1, (self.nn_nlayer_dim - 2 + 1)):
            self.z.append(self.a[i-1].dot(self.W[i]) + self.b[i])
            self.a.append(self.actFun(self.z[i], self.actFun_type))

        #Calculate softmax for output z:
        exp_scores = np.exp(self.z[-1])
        self.probs = exp_scores / np.sum(exp_scores, axis=1, keepdims=True)

        return None

    def backprop(self, X, y):
        '''
        backprop implements backpropagation to compute the gradients used to update the parameters in the backward step
        :param X: input data
        :param y: given labels
        :return: dL/dW1, dL/b1, dL/dW2, dL/db2
        '''

        delta = []
        dW = []
        db = []

        num_examples = len(X)
        delta.append(self.probs)
        delta[0][range(num_examples), y] -= 1
        dW.append((self.a[len(self.a) - 2].T).dot(delta[0]))
        db.append(np.sum(delta[0], axis=0, keepdims=True))

        if (self.nn_nlayer_dim>3):
            for i in range(1, self.nn_nlayer_dim-2):
                delta.append(delta[i-1].dot(self.W[len(self.W)-i].T) * self.diff_actFun(self.z[len(self.z)-i-1], self.actFun_type))
                dW.append(np.dot(self.a[len(self.a) - 2 - i].T, delta[i]))
                db.append(np.sum(delta[i], axis=0, keepdims=True))

        delta.append(delta[len(delta) - 1].dot(self.W[1].T) * self.diff_actFun(self.z[0], self.actFun_type))
        dW.append(np.dot(X.T, delta[-1]))
        db.append(np.sum(delta[len(delta) - 1], axis=0, keepdims=True))

        return dW, db

    def calculate_loss(self, X, y):
        '''
        calculate_loss computes the loss for prediction
        :param X: input data
        :param y: given labels
        :return: the loss for prediction
        '''
        num_examples = len(X)
        self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))

        # Calculating the loss
        data_loss_aux = -np.log(self.probs[range(num_examples), y])
        data_loss = np.sum(data_loss_aux)

        # Add regulatization term to loss (optional)
        reg_sum = 0
        for x in range(0, self.nn_nlayer_dim-1):
            reg_sum += np.sum(np.square(self.W[x]))
        data_loss += self.reg_lambda / 2 * reg_sum
        return (1. / num_examples) * data_loss

    def fit_model(self, X, y, Xtest, ytest, epsilon=0.001, num_passes=20000, print_loss=True):
        '''
        fit_model uses backpropagation to train the network
        :param X: input data
        :param y: given labels
        :param num_passes: the number of times that the algorithm runs through the whole dataset
        :param print_loss: print the loss or not
        :return:
        '''
        # Gradient descent.
        for i in range(0, num_passes):
            # Forward propagation
            self.feedforward(X, lambda x: self.actFun(x, type=self.actFun_type))
            # Backpropagation
            dW, db = self.backprop(X, y)

            # Add regularization terms (b's don't have regularization terms)
            for j in range(0, len(dW)):
                dW[j] += self.reg_lambda * self.W[self.nn_nlayer_dim-j-2]

            # Gradient descent parameter update
            for j in range(0, (self.nn_nlayer_dim - 1)):
                self.W[j] += -epsilon * dW[self.nn_nlayer_dim - j - 2]
                self.b[j] += -epsilon * db[self.nn_nlayer_dim - j - 2]

            # Optionally print the loss.
            # This is expensive because it uses the whole dataset, so we don't want to do it too often.
            # Also keep track of the loss for training and test sets:
            self.loss_training.append(self.calculate_loss(X, y))
            self.loss_test.append(self.calculate_loss(Xtest, ytest))
            if print_loss and i % 1000 == 0:
                print("Loss after iteration %i: %f" % (i, self.loss_training[-1]))

    def plot_loss_value(self):
        #Function to plot loss value per Epoch.
        plt.plot(self.loss_training)
        plt.plot(self.loss_test)
        plt.legend(['Training loss', 'Test loss'])
        plt.ylabel("Loss Value")
        plt.xlabel("Epoch")
        plt.show()


def main():
    ## generate and visualize Make-Moons dataset
    # X, y = generate_data(200)
    # Xtest, ytest = generate_data(10)
    # plt.scatter(X[:, 0], X[:, 1], s=40, c=y, cmap=plt.cm.Spectral)
    # plt.show()

    ##Create and train a neural network with fixed number of layers (3):
    # model2 = NeuralNetwork(nn_input_dim=2, nn_hidden_dim=3, nn_output_dim=2, actFun_type='Tanh')
    # model2.fit_model(X, y)
    # model2.visualize_decision_boundary(X, y)

    # ##Create and train a neural network with n layers, on the make_moons dataset:
    # model = n_layerNN(nn_nlayer_dim=12, nn_input_dim=2, nn_hidden_dim=10, nn_output_dim=2, actFun_type='ReLU')
    # model.fit_model(X, y, Xtest, ytest)
    # model.visualize_decision_boundary(X, y)
    # model.plot_loss_value()

    ## generate and visualize the new data set (circles)
    X2, y2 = generate_data_circles(200)
    X2test, y2test = generate_data_circles(10)
    # plt.scatter(X2[:, 0], X2[:, 1], s=40, c=y2, cmap=plt.cm.Spectral)
    # plt.show()

    #Create and train a neural network with n layers, on the make_circles dataset:
    model3 = n_layerNN(nn_nlayer_dim=5, nn_input_dim=2, nn_hidden_dim=10, nn_output_dim=2, actFun_type='Sigmoid')
    model3.fit_model(X2, y2, X2test, y2test)
    model3.visualize_decision_boundary(X2, y2)
    # model3.plot_loss_value()

if __name__ == "__main__":
    main()